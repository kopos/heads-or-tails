Get a Gif


Prequisites
- ImageMagick
- GIMP

How to
- Run the commands in gif2sprite.sh
- convert might always not be needed. So first check with montage and then if it does not the correct output use convert
- Use http://spritecutie.com/ where the sprite can be uploaded to get the CSS and HTML

References
- http://nick.onetwenty.org/index.php/2011/01/21/animated-gif-to-sprite-sheet-using-imagemagick/
